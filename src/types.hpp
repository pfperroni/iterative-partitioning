/**
 * Iterative Partitioning (c)
 *
 * Copyright 2015-2020 Peter Frank Perroni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional notifications, please check the file NOTICE.
 *
 *
 * @file types.hpp
 * @author Peter Frank Perroni
 * @brief Data structures.
 * @date Apr 30, 2015
 */

#ifndef TYPES_HPP_
#define TYPES_HPP_

#include "CEC13/Header.h"
#include <omp.h>
#include "config.hpp"

struct candidate{
	int dim;
	double *x;
	double fitness;
};

typedef struct s_particle : candidate {
	double *v;
	double *bestX;
	double bestFitness;
} particle;

typedef struct s_ringParticle : candidate {
	double fitness;
	double bestFitness;
	int bestLocalRingPart;
} ringParticle;

typedef struct s_swarm{
	int dim;
	int nParticle;
	int maxK;
	int K;
	char incTypeK;
	double incRateK;
	double currIncK;
	double boostIncK;
	int maxTries;
	int *dimMap; // Points to the particle's positions.
	int *kBounds; // Points to the mapping vector dimMap, not to the particles directly.
	int *ringLocalBest;
	double *x;
	double *xFitness;
	double *pBest;
	double *pFitness;
	double *kBest;
	double *kFitness;
	double *gBest;
	double gFitness;
} swarm;

typedef struct candidate agent;

typedef struct s_convergence_block{
	int pos;
	int nEvals[CONVERGENCE_BLOCK_SIZE];
	double nEvalsScaled[CONVERGENCE_BLOCK_SIZE];
	double fitness[CONVERGENCE_BLOCK_SIZE];
	double fitScaled[CONVERGENCE_BLOCK_SIZE];
} convergence_block;

struct fitness_function{
	char *name;
	double (*f)(candidate*);
	Benchmarks* F;
	int dim;
	double rangeX[2]; // [Min, Max] interval for dimensions.
	double *bestKnownPos;
	double bestKnownValue;
};

typedef struct _list_item{
	void *ptr;
	struct _list_item *previous;
	struct _list_item *next;
} list_item;

typedef struct list{
	int listType;
	int size;
	list_item *first;
	list_item *last;
} list;


#endif /* TYPES_HPP_ */
