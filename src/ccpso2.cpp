/**
 * Iterative Partitioning (c)
 *
 * Copyright 2015-2020 Peter Frank Perroni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional notifications, please check the file NOTICE.
 *
 *
 * @file ccpso2.cpp
 * @class ccpso2
 * @author Peter Frank Perroni
 * @brief Implementation of the CCPSO2-IP optimization algorithm.
 * @date Apr 2, 2015
 */

#include "ccpso2.hpp"

bool foundBestKnownValueCCPSO2;

void run_ccpso2(list *lBenchmarks, int *S, int szS, char boostType, int t, double boostInc, int nPart, int nIter, int maxEvals, int nExecs, int nDim){
	printf("Running CC Particle Swarm Optimization 2 with [#S=%i, b=%c%f, t=%i, nPart=%i, nIter=%i, nEvals=%i, nExecs=%i, nDim=%i]\n", szS, boostType, boostInc, t, nPart, nIter, maxEvals, nExecs, nDim);
	int szL = lBenchmarks->size;
	fitness_function *f[szL];
	int i = 0;
	for(list_item *it=lBenchmarks->first; it; it=it->next) f[i++] = (fitness_function*)it->ptr;
#pragma omp parallel for firstprivate(szL, S, szS, boostType, t, boostInc, nPart, nIter, maxEvals, nExecs, nDim) private(i) shared(f, lBenchmarks)
	for(i=0; i < szL; i++){
#pragma omp critical(control)
		printf("Thread #%i/%i processing %s\n", omp_get_thread_num(), omp_get_num_threads(), f[i]->name);
		ccpso2(f[i], S, szS, boostType, t, boostInc, nPart, nIter, maxEvals, nExecs, nDim);
	}
}

void ccpso2(fitness_function *fitFunc, int *S, int szS, char boostType, int t, double boostInc, int nPart, int nIter, int maxEvals, int nExecs, int nDim){
	ringParticle *part = createRingParticles(nPart, nDim);
	ringParticle *best = createRingParticles(1, nDim);
	ringParticle *candidate = createRingParticles(1, nDim);
	ringParticle *bestCandidate = createRingParticles(1, nDim);
	list *conv = createConvergenceList();
	double *sumConv = new double[nIter];
	int *sumEvals = new int[nIter];
	int *sumHits = new int[nIter];
	int *bestConvEvals = new int[nIter];
	double *bestConvValues = new double[nIter];
	double bestList[nExecs], worstFirstFit = 0, bestLastFit = WORST_VALUE;
	double bestFitness, currFitness, previousBestFitness;
	int i, j, p, d, k, pos, bestNeighbor, newDim, nEvals, ringPart[3], sz = 0, nEvalsBestKnownResults = INT_MAX;
	int countNoImprove, countRedimensioningsNoImprove;
	unsigned int seed;

	std::default_random_engine rneC, rneN;
	std::cauchy_distribution<double> C(1.0,0.2);
	std::normal_distribution<double> N(0.0, 1.0);

	swarm *s = NULL;

	memset(sumEvals, 0, nIter*sizeof(int));
	memset(sumHits, 0, nIter*sizeof(int));
	memset(sumConv, 0, nIter*sizeof(double));
//	for(i=0; i < nIter; i++) sumConv[i] = 0.0;
	best->bestFitness = WORST_VALUE;

	for(int exec=0; exec < nExecs; exec++){
		// Startup.
		seed = getRandomSeed(); // One totally random seed for independent run.
		previousBestFitness = bestFitness = WORST_VALUE;
		countNoImprove = countRedimensioningsNoImprove = nEvals = 0;
		foundBestKnownValueCCPSO2 = false;
		newDim = getNewDimension(seed, S, szS, boostInc, boostType, nIter, maxEvals, nDim, 0, 0);
		if(s == NULL) s = createSwarm(newDim, nPart, nDim, boostType, t, boostInc);
		else s->K = newDim;
		resetSwarm(seed, s, fitFunc->rangeX);
		DEBUG_TABLE_INT("swarm.dimMap", s->dimMap, 1, s->dim);
		DEBUG_TABLE_INT("swarm.kBounds", s->kBounds, s->K, 2);
		DEBUG_TABLE_DOUBLE("swarm.x", s->x, s->nParticle, s->dim);
		DEBUG_TABLE_DOUBLE("swarm.xFitness", s->xFitness, s->nParticle, s->K);
		DEBUG_TABLE_DOUBLE("swarm.pBest", s->pBest, s->nParticle, s->dim);
		DEBUG_TABLE_DOUBLE("swarm.pFitness", s->pFitness, s->nParticle, s->K);
		DEBUG_TABLE_INT("swarm.ringLocalBest", s->ringLocalBest, s->nParticle, s->K);
		DEBUG_TABLE_DOUBLE("swarm.kBest", s->kBest, 1, s->dim);
		DEBUG_TABLE_DOUBLE("swarm.kFitness", s->kFitness, 1, s->K);
		DEBUG_TABLE_DOUBLE("swarm.gBest", s->gBest, 1, s->dim);
		DEBUG_DOUBLE("swarm.gFitness", s->gFitness);

		// PSO Execution.
		for(i=0; !foundBestKnownValueCCPSO2 && i < nIter && nEvals < maxEvals; i++){ // For each Iteration.
			// Rearrange the dimensions.
			if(i > 1 && (bestFitness/previousBestFitness) > 0.8){ // If no sufficient improvement.
				if(szS > 0){ // If standard CCPSO2.
					s->K = getNewDimension(seed, S, szS, boostInc, boostType, nIter, maxEvals, nDim, i, nEvals);
					recreateSwarm(seed, s, s->K);
				}
				else{ // If CCPSO2-IP.
					countNoImprove++;
					if(countNoImprove == s->maxTries){
						countNoImprove = 0;
						int oldK = s->K;
						if(s->K <= MAX(ceil(s->maxK * s->currIncK), 1)){
							countRedimensioningsNoImprove++;
							if(countRedimensioningsNoImprove >= s->maxTries){ // Set maxK to maxK(0).
								countRedimensioningsNoImprove = 0;
								s->maxK = getNewDimension(seed, S, szS, boostInc, boostType, nIter, maxEvals, nDim, 0, 0);
							}
							else{// Set maxK to maxK(t).
								s->maxK = getNewDimension(seed, S, szS, boostInc, boostType, nIter, maxEvals, nDim, i, nEvals);
							}
							s->K = s->maxK;//ceil(MAX(s->maxK * (1 + s->currIncK), s->maxK + 1));
						}
						else{
							// Reduce K.
							s->K = MIN(ceil(MAX(s->K - MAX(s->maxK * s->currIncK, 1), 1)), s->maxK);
						}
						if(oldK != s->K) {
							recreateSwarm(seed, s, s->K);
							recalculatePBests(fitFunc, s, candidate);
						}
						else { // If swarm size is the same, permutate the dimensions.
							permutateSwarmDimensions(seed, s);
							recalculatePBests(fitFunc, s, candidate);
						}
						DEBUG_TABLE_INT("swarm.dimMap", s->dimMap, 1, s->dim);
						DEBUG_TABLE_INT("swarm.kBounds", s->kBounds, s->K, 2);
					}
					else if(RAND_DOUBLE(seed, 0, 1) < 0.5) { // Probabilistic permutation.
						permutateSwarmDimensions(seed, s);
						recalculatePBests(fitFunc, s, candidate);
					}
				}
			}
			else {
				countNoImprove = 0;
			}
			if(szS > 0){
				permutateSwarmDimensions(seed, s);
				recalculatePBests(fitFunc, s, candidate);
			}

			previousBestFitness = bestFitness;
			// Calculate fitness and update PBest and GBest.
			for(k=0; k < s->K; k++){ // For each partition.
				for(p=0; p < nPart; p++){ // For each particle.
					pos = (p * s->K) + k;
					s->xFitness[pos] = b(fitFunc, s, candidate, s->x, p, k);
					if(s->xFitness[pos] MIN_OR_MAX s->pFitness[pos]){ // Update pbest.
						for(d=s->kBounds[k*2]; d <= s->kBounds[k*2+1]; d++) s->pBest[(p*nDim)+s->dimMap[d]] = s->x[(p*nDim)+s->dimMap[d]];
						currFitness = s->pFitness[pos] = s->xFitness[pos];
					}
					if(s->pFitness[pos] MIN_OR_MAX s->kFitness[k]){ // Update kbest.
						for(d=s->kBounds[k*2]; d <= s->kBounds[k*2+1]; d++) s->kBest[s->dimMap[d]] = s->pBest[(p*nDim)+s->dimMap[d]];
						currFitness = s->kFitness[k] = s->pFitness[pos];
					}
					if(currFitness == fitFunc->bestKnownValue) foundBestKnownValueCCPSO2 = true;
				}
				DEBUG_TABLE_DOUBLE("swarm.pBest", s->pBest, s->nParticle, s->dim);
				DEBUG_TABLE_DOUBLE("swarm.pFitness", s->pFitness, s->nParticle, s->K);
				DEBUG_TABLE_DOUBLE("swarm.kBest", s->kBest, 1, s->dim);
				DEBUG_TABLE_DOUBLE("swarm.kFitness", s->kFitness, 1, s->K);
				for(p=0; p < nPart; p++){ // Update the ring local best.
					pos = (p * s->K) + k;
					ringPart[0] = (p==0) ? nPart-1 : p-1;
					ringPart[1] = p;
					ringPart[2] = (p==nPart-1) ? 0 : p+1;
					for(s->ringLocalBest[pos]=ringPart[0], j=1; j < 3; j++){
						if((s->pFitness[ringPart[j]*s->K+k] MIN_OR_MAX s->pFitness[s->ringLocalBest[pos]*s->K+k])){
							s->ringLocalBest[pos] = ringPart[j];
						}
					}
				}

				DEBUG_TABLE_INT("swarm.ringLocalBest", s->ringLocalBest, s->nParticle, s->K);
				if(s->kFitness[k] MIN_OR_MAX s->gFitness){ // Update gbest.
					memcpy(bestCandidate->x, s->gBest, nDim * sizeof(double));
					for(d=s->kBounds[k*2]; d <= s->kBounds[k*2+1]; d++) bestCandidate->x[s->dimMap[d]] = s->kBest[s->dimMap[d]];
					currFitness = callF(fitFunc, bestCandidate);

					if(currFitness MIN_OR_MAX s->gFitness){
						s->gFitness = bestFitness = currFitness;
						for(d=s->kBounds[k*2]; d <= s->kBounds[k*2+1]; d++) s->gBest[s->dimMap[d]] = s->kBest[s->dimMap[d]];
					}
					DEBUG_TABLE_DOUBLE("swarm.gBest", s->gBest, 1, s->dim);
					DEBUG_DOUBLE("swarm.gFitness", s->gFitness);
				}
			}
			// Update particle positions.
			for(k=0; k < s->K; k++){
				for(p=0; p < nPart; p++){
					pos = (p * s->dim);
					bestNeighbor = s->ringLocalBest[p * s->K + k] * s->dim;
					for(d=s->kBounds[k*2]; d <= s->kBounds[k*2+1]; d++){ // For each dimension.
						if(RAND_DOUBLE(seed, 0, 1) < 0.5){
							s->x[pos+s->dimMap[d]] = s->pBest[pos+s->dimMap[d]] +
									C(rneC) * fabs(s->pBest[pos+s->dimMap[d]] - s->pBest[bestNeighbor+s->dimMap[d]]);
						}
						else{
							s->x[pos+s->dimMap[d]] = s->pBest[bestNeighbor+s->dimMap[d]] +
									N(rneN) * fabs(s->pBest[pos+s->dimMap[d]] - s->pBest[bestNeighbor+s->dimMap[d]]);
						}
						if(s->x[pos+s->dimMap[d]] < fitFunc->rangeX[0])
							s->x[pos+s->dimMap[d]] =  fitFunc->rangeX[0];
						else if(s->x[pos+s->dimMap[d]] > fitFunc->rangeX[1])
							s->x[pos+s->dimMap[d]] =  fitFunc->rangeX[1];
					}
				}
			}
			DEBUG_TABLE_DOUBLE("swarm.x", s->x, s->nParticle, s->dim);
			nEvals += nPart * s->K;
			addConvergence(conv, nEvals, bestFitness);
		}
		// Save the best value from all independ runs.
		if(s->gFitness == fitFunc->bestKnownValue || s->gFitness MIN_OR_MAX best->bestFitness){
			best->bestFitness = s->gFitness;
			memcpy(best->x, s->gBest, nDim * sizeof(double));
			memset(bestConvEvals, 0, nIter*sizeof(int));
			memset(bestConvValues, 0, nIter*sizeof(double));
			copyConvergence(conv, bestConvEvals, bestConvValues);
		}
		if(foundBestKnownValueCCPSO2 && nEvals < nEvalsBestKnownResults) nEvalsBestKnownResults = nEvals;
		sumConvergence(conv, sumEvals, sumHits, sumConv, worstFirstFit, bestLastFit);
		bestList[exec] = bestFitness;
		sz = (i > sz) ? i : sz;
		emptyList(conv);
	}

// Generate the report.
#pragma omp critical(report)
{
	printf("\nCalculating %s (%i executions) with %i dimensions\n", fitFunc->name, nExecs, nDim);

	if(nEvalsBestKnownResults < INT_MAX) printf("Best known result found at %i fitness evaluations\n", nEvalsBestKnownResults);
	printf("Last evaluation checked: %i\n", sz);
	printf("Avg. Convergence: [\n");
	for(i=0; i < sz; i++){
		sumConv[i] /= sumHits[i];
		sumEvals[i] /= sumHits[i];
		printf("%i,%lf\n", sumEvals[i], sumConv[i]);
	}
	printf("]\n");
	printf("Worst First Fitness: %lf; Best Last Fitness: %lf; Avg. Best Fitness: %lf +-%lf ; Known Global Best=%f\n",
				worstFirstFit, bestLastFit, calc_media(bestList, nExecs), calc_desvpad(bestList, nExecs), fitFunc->bestKnownValue);

	printf("Best Convergence: [\n");
	for(i=0; i < sz && bestConvEvals[i] > 0; i++){
		printf("%i,%lf\n", bestConvEvals[i], bestConvValues[i]);
	}
	printf("]\n");

	printf("GBests: [\n");
	char name[4];
	for(name[3]='\0', i=0; i < nExecs; i++){
		name[0] = fitFunc->name[0]; name[1] = fitFunc->name[1]; name[2] = fitFunc->name[2];
		printf("%s,%i,%lf\n", name, i, bestList[i]);
	}
	printf("]\n");

	printf("Best result found: \n\tfitness=[%f]\nDimensions=[ ", best->bestFitness);
	for(d=0; d < nDim; d++){
		printf(" %lf ", best->x[d]);
	}
	printf(" ]\n");
	printf("Known Best Result: \n[ ");
	for(d=0; d < nDim; d++){
		printf(" %lf ", fitFunc->bestKnownPos[d]);
	}
	printf(" ]\n");
	fflush(stdout);

	deleteRingParticles(part, nPart);
	deleteRingParticles(best, 1);
	deleteRingParticles(candidate, 1);
	deleteRingParticles(bestCandidate, 1);
	destroyList(conv);
	deleteSwarm(s);
	delete sumHits;
	delete sumEvals;
	delete sumConv;
	delete bestConvEvals;
	delete bestConvValues;
}
}

swarm* createSwarm(int K, int nPart, int nDim, char boostType, int t, double boostInc){
	swarm *s = new swarm;
	s->dim = nDim;
	s->nParticle = nPart;
	s->maxK = K;
	s->K = K;
	s->maxTries = t;
	s->incTypeK = (char)boostType;  // {'L', 'S', 'E'}
	s->boostIncK = boostInc; // [0, 1]
	s->currIncK = 1.0 / s->maxTries;
	s->dimMap = new int[nDim];
	s->x = new double[nPart * nDim];
	s->pBest = new double[nPart * nDim];
	s->kBest = new double[nDim];
	s->gBest = new double[nDim];
	// Reduce overhead by setting its size to the maximum allowed (nDim*2), instead of K.
	s->kBounds = new int[nDim * 2];
	s->ringLocalBest = new int[nPart * nDim];
	s->xFitness = new double[nPart * nDim];
	s->pFitness = new double[nPart * nDim];
	s->kFitness = new double[nDim];
	return s;
}

void deleteSwarm(swarm *s){
	delete s->kBounds;
	delete s->dimMap;
	delete s->ringLocalBest;
	delete s->x;
	delete s->xFitness;
	delete s->pBest;
	delete s->pFitness;
	delete s->kBest;
	delete s->kFitness;
	delete s->gBest;
	delete s;
}

void resetSwarm(unsigned int seed, swarm *s){
	resetSwarm(seed, s, NULL, false);
}

void resetSwarm(unsigned int seed, swarm *s, double *rangeX){
	resetSwarm(seed, s, rangeX, true);
}

void resetSwarm(unsigned int seed, swarm *s, double *rangeX, bool resetAll){
	int i, d, p;
	for(i=0; i < s->nParticle*s->K; i++){
		// Not needed to reset s->xFitness[i], since it's Always recalculated.
		s->pFitness[i] = WORST_VALUE;
	}
	for(d=-1, i=0; i < s->K; i++){
		s->kFitness[i] = WORST_VALUE;
		if(i == s->K-1){
			s->kBounds[i*2] = d + 1;
			s->kBounds[i*2+1] = s->dim-1;
		}
		else{
			s->kBounds[i*2] = d + 1;
			d += s->dim / s->K;
			s->kBounds[i*2+1] = d;
		}
	}
	for(d=0; d < s->dim; d++){
		s->dimMap[d] = d;
	}
	if(resetAll){
		s->gFitness = WORST_VALUE;
		for(d=0; d < s->dim; d++){
			s->kBest[d] = RAND_DOUBLE(seed, rangeX[0], rangeX[1]);
			s->gBest[d] = RAND_DOUBLE(seed, rangeX[0], rangeX[1]);
		}
		for(p=0; p < s->nParticle; p++){
			for(d=0; d < s->dim; d++){
				s->x[(p*s->dim)+d] = RAND_DOUBLE(seed, rangeX[0], rangeX[1]);
				s->pBest[(p*s->dim)+d] = RAND_DOUBLE(seed, rangeX[0], rangeX[1]);
			}
		}
	}
}

void recreateSwarm(unsigned int seed, swarm *s, int K){
	s->K = K;
	resetSwarm(seed, s);
}

void permutateSwarmDimensions(unsigned int seed, swarm *s){
	int i, d, sum, pos;
	int map[s->dim];
	for(i=0; i < s->dim; i++) map[i] = i;
	memset(s->dimMap, 0, s->dim * sizeof(int));
	for(d=0; d < s->dim; d++){
		pos = RAND_INT(seed, 0, s->dim-1);
		for(i=pos; map[i] == -1 && i < s->dim; i++);
		if(i == s->dim){
			for(i=pos-1; map[i] == -1 && i >= 0; i--);
		}
		s->dimMap[d] = map[i];
		map[i] = -1;
	}
	for(sum=0, i=0; i < s->K; i++){
		if(i == s->K-1){
			s->kBounds[i*2] = sum;
			s->kBounds[i*2+1] = s->dim-1;
		}
		else{
			d = RAND_INT(seed, 1, s->dim - (s->K - i - 1) - sum);
			s->kBounds[i*2] = sum;
			s->kBounds[i*2+1] = sum + d - 1;
			sum += d;
		}
	}
}

void recalculatePBests(fitness_function *fitFunc, swarm *s, ringParticle *candidate){
	for(int pos, p, k=0; k < s->K; k++){
		s->kFitness[k] = WORST_VALUE;
		for(p=0; p < s->nParticle; p++){
			pos = (p * s->K) + k;
			s->pFitness[pos] = b(fitFunc, s, candidate, s->pBest, p, k);
			if(s->pFitness[pos] MIN_OR_MAX s->kFitness[k]){
				s->kFitness[k] = s->pFitness[pos];
			}
		}
	}
}

double b(fitness_function *fitFunc, swarm *s, ringParticle *candidate, double *m, int partPos, int k){
	for(int i, _k=0; _k < s->K; _k++){
		for(i=s->kBounds[_k*2]; i <= s->kBounds[_k*2+1]; i++){
			candidate->x[s->dimMap[i]] = (_k == k) ? m[(partPos*s->dim)+s->dimMap[i]] : s->kBest[s->dimMap[i]];
		}
	}
	return callF(fitFunc, candidate);
}

ringParticle* createRingParticles(int nPart, int nDim){
	ringParticle *part = new ringParticle[nPart];
	for(int i=0; i < nPart; i++){
		part[i].dim = nDim;
		part[i].x = new double[nDim];
	}
	return part;
}

void deleteRingParticles(ringParticle *part, int nPart){
	for(int i=0; i < nPart; i++){
		delete part[i].x;
	}
	delete part;
}

int getNewDimension(unsigned int seed, int *S, int szS, double boostInc, char boostType, int nIter, int maxEvals, int nDim, int currIter, int currEvals){
	if(szS > 0) return nDim / S[RAND_INT(seed, 0, szS)];
	double rate;
	if(boostType == 'L') rate = -MAX((double)currIter/nIter, (double)currEvals/maxEvals) * boostInc + boostInc;
	else if(boostType == 'S') rate = (boostInc / (1.0 + exp((12*boostInc) * MAX((double)currIter/nIter, (double)currEvals/maxEvals) - 6*boostInc)));
	else if(boostType == 'E') rate = boostInc / exp((12*boostInc) * MAX((double)currIter/nIter, (double)currEvals/maxEvals));
	return MIN(MAX(ceil(nDim * rate), 1), nDim);
}
