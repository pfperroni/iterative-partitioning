# Run the command bellow if you need multithreading. Set the number of processors you want to be using in parallel (in the example below, 4 processors):
export OMP_NUM_THREADS=4

#-F:500K
nohup ./benchmarks pso -C:C -i:500000 -f:500000 -e:10 -d:1000 -p:15 -w:0.7 -c1:0.8 -c2:1.1 > pso-d_1000-e_10-p_15-500K-w_0.7-c1_0.8-c2_1.1.out &
nohup ./benchmarks ccpso2 -C:C -i:500000 -f:500000 -e:10 -d:1000 -p:15 -S:2,5,10,50,100,250 > ccpso2-d_1000-e_10-p_15-500K-std.out &
nohup ./benchmarks ccpso2 -C:C -i:500000 -f:500000 -e:10 -d:1000 -p:15 -b:S0.521 -t:3 > ccpso2-d_1000-e_10-p_15-t_3-500K-S-b_0.521.out &
nohup ./benchmarks ccpso2 -C:C -i:500000 -f:500000 -e:10 -d:1000 -p:15 -b:E0.5 -t:2 > ccpso2-d_1000-e_10-p_15-t_2-500K-E-b_0.5.out &
nohup ./benchmarks ccpso2 -C:C -i:500000 -f:500000 -e:10 -d:1000 -p:15 -b:L0.5 -t:5 > ccpso2-d_1000-e_10-p_15-t_5-500K-L-b_0.5.out &

#-F:1M
nohup ./benchmarks pso -C:C -i:1000000 -f:1000000 -e:25 -d:1000 -p:30 -w:0.7 -c1:0.8 -c2:1.1 > pso-d_1000-e_25-p_30-1M-w_0.7-c1_0.8-c2_1.1.out &
nohup ./benchmarks ccpso2 -C:C -i:1000000 -f:1000000 -e:25 -d:1000 -p:30 -S:2,5,10,50,100,250 > ccpso2-d_1000-e_25-p_30-1M-std.out &
nohup ./benchmarks ccpso2 -C:C -i:1000000 -f:1000000 -e:25 -d:1000 -p:30 -b:S0.521 -t:3 > ccpso2-d_1000-e_25-p_30-t_3-1M-S-b_0.521.out &
nohup ./benchmarks ccpso2 -C:C -i:1000000 -f:1000000 -e:25 -d:1000 -p:30 -b:E0.5 -t:2 > ccpso2-d_1000-e_25-p_30-t_2-1M-E-b_0.5.out &
nohup ./benchmarks ccpso2 -C:C -i:1000000 -f:1000000 -e:25 -d:1000 -p:30 -b:L0.5 -t:5 > ccpso2-d_1000-e_25-p_30-t_5-1M-L-b_0.5.out &
