/**
 * Iterative Partitioning (c)
 *
 * Copyright 2015-2020 Peter Frank Perroni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional notifications, please check the file NOTICE.
 *
 *
 * @file ccpso2.hpp
 * @class ccpso2
 * @author Peter Frank Perroni
 * @brief Implementation of the CCPSO2-IP optimization algorithm.
 * @date Apr 2, 2015
 */

#include "bench_func.hpp"
#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include <time.h>
#include <math.h>
#include <limits.h>
#include <random>

void run_ccpso2(list *lBenchmarks, int *S, int szS, char r, int t, double b, int nPart, int nIter, int maxEvals, int nExecs, int nDim);
void ccpso2(fitness_function *fitFunc, int *S, int szS, char r, int t, double b, int nPart, int nIter, int maxEvals, int nExecs, int nDim);

double calcExpDiff(list *l, int &tau, double &alfa1, double &alfa2, int printCfg);

swarm* createSwarm(int K, int nPart, int nDim, char boostType, int t, double boostInc);
void deleteSwarm(swarm *s);
void resetSwarm(unsigned int seed, swarm *s);
void resetSwarm(unsigned int seed, swarm *s, double *rangeX);
void resetSwarm(unsigned int seed, swarm *s, double *rangeX, bool resetAll);
void recreateSwarm(unsigned int seed, swarm *s, int K);
void permutateSwarmDimensions(unsigned int seed, swarm *s);
int getNewDimension(unsigned int seed, int *S, int szS, double boostInc, char incType, int nIter, int maxEvals, int nDim, int currIter, int currEvals);
void recalculatePBests(fitness_function *fitFunc, swarm *s, ringParticle *candidate);
double b(fitness_function *fitFunc, swarm *s, ringParticle *candidate, double *x, int partPos, int k);
ringParticle* createRingParticles(int nPart, int nDim);
void deleteRingParticles(ringParticle *part, int nPart);
