/**
 * Iterative Partitioning (c)
 *
 * Copyright 2015-2020 Peter Frank Perroni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional notifications, please check the file NOTICE.
 *
 *
 * @file benchmark.cpp
 * @author Peter Frank Perroni
 * @brief Iterative Partitioning root file.
 * @date Apr 2, 2015
 */

#include "tools.hpp"
#include "pso.hpp"
#include "ccpso2.hpp"

int main(int argc, char *argv[]){
	if(argc < 2) return 1;

	int nExecs = getIntParam("-e:", argc, argv, 10); // Number of independent executions.
	int nIter = getIntParam("-i:", argc, argv, 10); // Number of Iterations.
	int maxEvals = getIntParam("-f:", argc, argv, 1000); // Maximum number of function evaluations.
	int nDim = getIntParam("-d:", argc, argv, 3); // Number of dimensions.
	char C = getCharParam("-C:", argc, argv, 'C'); // Type of benchmark [C=Competition for 1K dimensions, S=Standard].

	list *lBenchmarks = createBenchmarkList();
	if(C == 'C'){ // 'C' -> Competition
		addItem(lBenchmarks, getCEC15_F1(nDim));
		addItem(lBenchmarks, getCEC15_F2(nDim));
		addItem(lBenchmarks, getCEC15_F3(nDim));
		addItem(lBenchmarks, getCEC15_F4(nDim));
		addItem(lBenchmarks, getCEC15_F5(nDim));
		addItem(lBenchmarks, getCEC15_F6(nDim));
		addItem(lBenchmarks, getCEC15_F7(nDim));
		addItem(lBenchmarks, getCEC15_F8(nDim));
		addItem(lBenchmarks, getCEC15_F9(nDim));
		addItem(lBenchmarks, getCEC15_F10(nDim));
		addItem(lBenchmarks, getCEC15_F11(nDim));
		addItem(lBenchmarks, getCEC15_F12(nDim));
		addItem(lBenchmarks, getCEC15_F13(nDim));
		addItem(lBenchmarks, getCEC15_F14(nDim));
		addItem(lBenchmarks, getCEC15_F15(nDim));
	}
	else{ // 'S' -> Standard.
		addItem(lBenchmarks, getMatyas(nDim));
		addItem(lBenchmarks, getSphere(nDim));
		addItem(lBenchmarks, getAckleys(nDim));
		addItem(lBenchmarks, getSchaffer2(nDim));
		addItem(lBenchmarks, getLevi(nDim));
		addItem(lBenchmarks, getRosenbrock(nDim));
		addItem(lBenchmarks, getRastrigin(nDim));
	}

	if(strcmp(argv[1], "pso") == 0){
		int nPart = getIntParam("-p:", argc, argv, 50);
		double w = getDoubleParam("-w:", argc, argv, 0.7);
		double c1 = getDoubleParam("-c1:", argc, argv, 0.8);
		double c2 = getDoubleParam("-c2:", argc, argv, 0.9);
		run_pso(lBenchmarks, w, c1, c2, nPart, nIter, maxEvals, nExecs, nDim);
	}
	else if(strcmp(argv[1], "ccpso2") == 0){ //ccpso2 -C:S -i:1000000 -f:10000 -e:10 -d:1000 -p:15 -b:E1 -t:2 ->S:2,5,10,50,100,250 -w:0.7 -c1:0.8 -c2:0.9
		char boostType;
		double b;
		int S[nDim]; // Maximum allowed size for S.
		int szS = getIntListParam("-S:", argc, argv, S); // Random swarms sizes. If present, CCPSO2 will be executed. Otherwise, CCPSO2-IP.
		int nPart = getIntParam("-p:", argc, argv, 50); // Number of particles.
		int t = getIntParam("-t:", argc, argv, 3);  // Number of tries.
		getCharAndDoubleParam("-b:", argc, argv, &boostType, &b);  // Boost function:Boost rate.
		run_ccpso2(lBenchmarks, S, szS, boostType, t, b, nPart, nIter, maxEvals, nExecs, nDim);
	}

	destroyList(lBenchmarks);

	return 0;
}
