/**
 * Iterative Partitioning (c)
 *
 * Copyright 2015-2020 Peter Frank Perroni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional notifications, please check the file NOTICE.
 *
 *
 * @file pso.hpp
 * @class pso
 * @author Peter Frank Perroni
 * @brief Implementation of the PSO optimization algorithm.
 * @date Apr 2, 2015
 */

#include "bench_func.hpp"
#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include <time.h>
#include <math.h>
#include <limits.h>

void run_pso(list *lBenchmarks, float w, float c1, float c2, int nPart, int nIter, int maxEvals, int nExecs, int nDim);
void pso(fitness_function *fitFunc, float w, float c1, float c2, int nPart, int nIter, int maxEvals, int nExecs, int nDim);
particle* createParticles(int nPart, int nDim);
void deleteParticles(particle *part, int nPart);
