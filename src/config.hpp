/**
 * Iterative Partitioning (c)
 *
 * Copyright 2015-2020 Peter Frank Perroni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional notifications, please check the file NOTICE.
 *
 *
 * @file config.hpp
 * @author Peter Frank Perroni
 * @brief Compilation macros.
 * @date Apr 30, 2015
 */

#ifndef CONFIG_HPP_
#define CONFIG_HPP_

//#define DEBUG

#define CONVERGENCE_BLOCK_SIZE 1000

#define MIN_OR_MAX <
#define NOT_MIN_OR_MAX >=
#define WORST_VALUE DBL_MAX

#endif /* CONFIG_HPP_ */
