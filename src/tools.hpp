/**
 * Iterative Partitioning (c)
 *
 * Copyright 2015-2020 Peter Frank Perroni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional notifications, please check the file NOTICE.
 *
 *
 * @file tools.hpp
 * @author Peter Frank Perroni
 * @brief Helper functions.
 * @date Apr 30, 2015
 */

#ifndef TOOLS_HPP_
#define TOOLS_HPP_

#include "types.hpp"
#include "macros.hpp"
#include <float.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void deleteFitFuncStruct(fitness_function *ff, int nDim);

void addConvergence(list *l, int nEvals, double fitness);
void emptyList(list *l);
void destroyList(list *l);
list *createList();
list *createConvergenceList();
list *createBenchmarkList();
void insertItem(list *l, void *ptr, int pos);
void addItem(list *l, void *ptr);
double calc_media(double *val, int tamanho);
double calc_variancia(double *val, int tamanho);
double calc_desvpad(double *val, int tamanho);
void sumConvergence(list *l, int *addValuesToEvals, int *addValuesToHits, double *addValuesTo, double &worstFit, double &bestFit);
void copyConvergence(list *l, int *evals, double *values);
double getDoubleParam(const char *opt, int argc, char *argv[], double defaultValue);
int getIntParam(const char *opt, int argc, char *argv[], int defaultValue);
int getIntListParam(const char *opt, int argc, char *argv[], int *list);
char getCharParam(const char *opt, int argc, char *argv[], char defaultValue);
void getCharAndDoubleParam(const char *opt, int argc, char *argv[], char *c, double *v);
unsigned int getRandomSeed();

#endif /* TOOLS_HPP_ */
