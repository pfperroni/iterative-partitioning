/**
 * Iterative Partitioning (c)
 *
 * Copyright 2015-2020 Peter Frank Perroni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional notifications, please check the file NOTICE.
 *
 *
 * @file pso.cpp
 * @class pso
 * @author Peter Frank Perroni
 * @brief Implementation of the PSO optimization algorithm.
 * @date Apr 2, 2015
 */

#include "pso.hpp"

bool foundBestKnownValue;

void run_pso(list *lBenchmarks, float w, float c1, float c2, int nPart, int nIter, int maxEvals, int nExecs, int nDim){
	printf("Running Particle Swarm Optimization with [w=%f, c1=%f, c2=%f, nPart=%i, nIter=%i, nEvals=%i, nExecs=%i, nDim=%i]\n", w, c1, c2, nPart, nIter, maxEvals, nExecs, nDim);

	fitness_function *f;
	for(list_item *it=lBenchmarks->first; it; it=it->next){
		f = (fitness_function*)it->ptr;
		pso(f, w, c1, c2, nPart, nIter, maxEvals, nExecs, nDim);
	}
}

void pso(fitness_function *fitFunc, float w, float c1, float c2, int nPart, int nIter, int maxEvals, int nExecs, int nDim){
	particle *part = createParticles(nPart, nDim);
	particle *best = createParticles(1, nDim);
	list *conv = createConvergenceList();
	double *sumConv = new double[nIter];
	int *sumEvals = new int[nIter];
	int *sumHits = new int[nIter];
	double bestList[nExecs], worstFirstFit = 0, bestLastFit = WORST_VALUE;
	double bestFitness, currFitness, currW;
	int i, p, d, nEvals, sz = 0, nEvalsBestKnownResults = INT_MAX;
	unsigned int seedStartup, seed1, seed2;

	memset(sumEvals, 0, nIter*sizeof(int));
	memset(sumHits, 0, nIter*sizeof(int));
	memset(sumConv, 0, nIter*sizeof(double));

	printf("Calculating %s (%i executions) with %i dimensions\n", fitFunc->name, nExecs, nDim);
	for(int exec=0; exec < nExecs; exec++){
		seedStartup = getRandomSeed();
		seed1 = getRandomSeed();
		seed2 = getRandomSeed();
		bestFitness = DBL_MAX;
		best->fitness = DBL_MAX;
		currW = w;
		nEvals = 0;
		foundBestKnownValue = false;
		// Startup.
		for(p=0; p < nPart; p++){
			for(d=0; d < nDim; d++){
				part[p].x[d] = RAND_DOUBLE(seedStartup, fitFunc->rangeX[0], fitFunc->rangeX[1]);
				part[p].v[d] = RAND_DOUBLE(seedStartup, fitFunc->rangeX[0] / 10, fitFunc->rangeX[1] / 10);
			}
			part[p].bestFitness = DBL_MAX;
			part[p].fitness = currFitness = callF(fitFunc, &part[p]);
			if(currFitness MIN_OR_MAX part[p].bestFitness && currFitness >= 0) SAVE_PART(part[p]);
			if(currFitness MIN_OR_MAX bestFitness && currFitness >= 0){
				COPY_PART(part[p], best);
				bestFitness = best->bestFitness;
			}
			if(currFitness == fitFunc->bestKnownValue) foundBestKnownValue = true;
		}
		nEvals += nPart;
		addConvergence(conv, nEvals, bestFitness);

		// PSO Execution.
		for(i=0; !foundBestKnownValue && i < nIter && nEvals < maxEvals; i++){ // For each Iteration.
			for(p=0; p < nPart; p++){ // For each particle.
				for(d=0; d < nDim; d++){ // For each dimension.
					part[p].v[d] = currW*part[p].v[d] +
							c1 * RAND_DOUBLE(seed1, 0, 1) * (best->x[d] - part[p].x[d]) +
							c2 * RAND_DOUBLE(seed2, 0, 1) * (part[p].bestX[d] - part[p].x[d]);
					if((part[p].x[d]+part[p].v[d]) < fitFunc->rangeX[0])
						part[p].v[d] = fitFunc->rangeX[0] - part[p].x[d];
					else if((part[p].x[d]+part[p].v[d]) > fitFunc->rangeX[1])
						part[p].v[d] =  part[p].x[d] - fitFunc->rangeX[1];
					part[p].x[d] += part[p].v[d];
				}
				part[p].fitness = currFitness = callF(fitFunc, &part[p]);
				if(currFitness MIN_OR_MAX part[p].bestFitness && currFitness >= 0) SAVE_PART(part[p]);
				if(currFitness MIN_OR_MAX bestFitness && currFitness >= 0){
					COPY_PART(part[p], best);
					bestFitness = best->bestFitness;
				}
				if(currFitness == fitFunc->bestKnownValue) foundBestKnownValue = true;
			}
			nEvals += nPart;
			addConvergence(conv, nEvals, bestFitness);
			currW -= w/nIter;
		}
		if(foundBestKnownValue && nEvals < nEvalsBestKnownResults) nEvalsBestKnownResults = nEvals;
		sumConvergence(conv, sumEvals, sumHits, sumConv, worstFirstFit, bestLastFit);
		bestList[exec] = bestFitness;
		sz = (i > sz) ? i : sz;
		emptyList(conv);
	}

	// Generate report.
	if(nEvalsBestKnownResults < INT_MAX) printf("Best known result found at %i fitness evaluations\n", nEvalsBestKnownResults);
	printf("Last evaluation checked: %i\n", sz);
	printf("Avg. Convergence: [\n");
	for(i=0; i < sz; i++){
		sumConv[i] /= sumHits[i];
		sumEvals[i] /= sumHits[i];
		printf("%i,%lf\n", sumEvals[i], sumConv[i]);
	}
	printf("]\n");
	printf("Worst First Fitness: %lf; Best Last Fitness: %lf; Avg. Best Fitness: %lf +-%lf ; Known Global Best=%f\n",
				worstFirstFit, bestLastFit, calc_media(bestList, nExecs), calc_desvpad(bestList, nExecs), fitFunc->bestKnownValue);

	printf("Best result found: \n\tfitness=[%f]\nDimensions=[ ", best->bestFitness);
	for(d=0; d < nDim; d++){
		printf(" %lf ", best->bestX[d]);
	}
	printf(" ]\n");
	printf("Known Best Result: \n[ ");
	for(d=0; d < nDim; d++){
		printf(" %lf ", fitFunc->bestKnownPos[d]);
	}
	printf(" ]\n");

	deleteParticles(part, nPart);
	deleteParticles(best, 1);
	destroyList(conv);
	delete sumConv;
	delete sumHits;
	delete sumEvals;
}

particle* createParticles(int nPart, int nDim){
	particle *part = new particle[nPart];
	for(int i=0; i < nPart; i++){
		part[i].dim = nDim;
		part[i].x = new double[nDim];
		part[i].v = new double[nDim];
		part[i].bestX = new double[nDim];
	}
	return part;
}

void deleteParticles(particle *part, int nPart){
	for(int i=0; i < nPart; i++){
		delete part[i].x;
		delete part[i].v;
		delete part[i].bestX;
	}
	delete part;
}

