/**
 * Iterative Partitioning (c)
 *
 * Copyright 2015-2020 Peter Frank Perroni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional notifications, please check the file NOTICE.
 *
 *
 * @file macros.hpp
 * @author Peter Frank Perroni
 * @brief Macro definitions.
 * @date Apr 30, 2015
 */

#ifndef MACROS_HPP_
#define MACROS_HPP_

enum { LIST_ITEM_CONVERGENCE, LIST_ITEM_FITNESSFUNCTION };
enum { CALC_MAX_ITER, CALIBRATE, VALIDATE };
enum { PRINT_NONE, PRINT_BASIC, PRINT_CONVERGENCE, PRINT_ALL };

#ifndef MAX
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef sign
#define sign(exp) (((exp)>=0.0) ? 1 : -1)
#endif

#define pi 3.1415926535897932
//#define RAND_DOUBLE(a, b) ((a==b)?a:(a+(((double)rand())/RAND_MAX)*(b-a)))
//#define RAND_INT(a, b) (int)RAND_DOUBLE(a, b)
#define RAND_DOUBLE(seed, a, b) ((a==b)?a:(a+(((double)rand_r(&seed))/RAND_MAX)*(b-a)))
#define RAND_INT(seed, a, b) (int)RAND_DOUBLE(seed, a, b)

#define SAVE_PART(p) {\
	for(int _d_=0; _d_ < p.dim; _d_++){ \
		p.bestX[_d_] = p.x[_d_]; \
	} \
	p.bestFitness = p.fitness; \
}


#define COPY_PART(pOrig, pDest) { \
	for(int _d_=0; _d_ < pOrig.dim; _d_++) { \
		pDest->x[_d_] = pOrig.x[_d_]; \
		pDest->v[_d_] = pOrig.v[_d_]; \
		pDest->bestX[_d_] = pOrig.bestX[_d_]; \
	} \
	pDest->fitness = pOrig.fitness; \
	pDest->bestFitness = pOrig.bestFitness; \
}

#define COPY_AGENT(aOrig, aDest) { \
	for(int _d_=0; _d_ < aOrig.dim; _d_++) { \
		aDest->x[_d_] = aOrig.x[_d_]; \
	} \
	aDest->fitness = aOrig.fitness; \
}

#ifdef DEBUG
#define DEBUG_TABLE_INT(label, table, rows, cols) \
	printf("%s: {\n", label); \
	for(int _j_, _i_=0; _i_ < rows; _i_++){ \
		printf(" ["); \
		for(_j_=0; _j_ < cols; _j_++) \
			printf(" %i ", table[(_i_*cols)+_j_]); \
		printf("]\n"); \
	} \
	printf("}\n");
#define DEBUG_TABLE_DOUBLE(label, table, rows, cols) \
	printf("%s: {\n", label); \
	for(int _j_, _i_=0; _i_ < rows; _i_++){ \
		printf(" ["); \
		for(_j_=0; _j_ < cols; _j_++) \
			printf(" %f ", table[(_i_*cols)+_j_]); \
		printf("]\n"); \
	} \
	printf("}\n");
#define DEBUG_INT(label, value) printf("%s: %i\n", label, s->gFitness);
#define DEBUG_DOUBLE(label, value) printf("%s: %f\n", label, s->gFitness);
#else
#define DEBUG_TABLE_INT(label, table, rows, cols)
#define DEBUG_TABLE_DOUBLE(label, table, rows, cols)
#define DEBUG_INT(label, value)
#define DEBUG_DOUBLE(label, value)
#endif


#endif /* MACROS_HPP_ */
