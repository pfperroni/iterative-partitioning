/**
 * Iterative Partitioning (c)
 *
 * Copyright 2015-2020 Peter Frank Perroni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional notifications, please check the file NOTICE.
 *
 *
 * @file bench_func.cpp
 * @author Peter Frank Perroni
 * @brief Implementation of benchmark functions.
 * @date Apr 30, 2015
 */

#include "bench_func.hpp"

// http://en.wikipedia.org/wiki/Test_functions_for_optimization
// http://www.cs.cmu.edu/afs/cs/project/jair/pub/volume24/ortizboyer05a-html/node6.html

double callF(fitness_function *f, candidate *candidate){
	if(f->F != NULL) return f->F->compute(candidate->x);
	return f->f(candidate);
}

fitness_function* createFitFuncStruct(const char *name, double (*f)(candidate*), double rangeMin, double rangeMax, double bestKnownPos, int nDim, double bestKnownValue){
	return createFitFuncStruct(name, f, NULL, rangeMin, rangeMax, bestKnownPos, nDim, bestKnownValue);
}

fitness_function* createFitFuncStruct(const char *name, Benchmarks *F, double rangeMin, double rangeMax, double bestKnownPos, int nDim, double bestKnownValue){
	return createFitFuncStruct(name, NULL, F, rangeMin, rangeMax, bestKnownPos, nDim, bestKnownValue);
}

fitness_function* createFitFuncStruct(const char *name, double (*f)(candidate*), Benchmarks *F, double rangeMin, double rangeMax, double bestKnownPos, int nDim, double bestKnownValue){
	fitness_function *ff = new fitness_function;
	ff->name = (char*)name;
	ff->f = f;
	ff->F = F;
	ff->dim = nDim;
	ff->rangeX[0] = rangeMin; ff->rangeX[1] = rangeMax;
	ff->bestKnownValue = bestKnownValue;
	ff->bestKnownPos = new double[nDim];
	for(int i=0; i < nDim; i++){
		ff->bestKnownPos[i] = bestKnownPos;
	}
	return ff;
}

void deleteFitFuncStruct(fitness_function *ff, int nDim){
	delete ff->bestKnownPos;
	delete ff;
}

fitness_function* getRosenbrock(int nDim){
	return createFitFuncStruct("rosenbrock", &rosenbrock, -2.048, 2.048, 1, nDim, 0);
}
double rosenbrock(candidate *c){
	// ((1-x)^2)+(100*((y-(x^2))^2)): Global Best at (1,1)=0, Range[-2.048, 2.048]
	c->fitness = 0;
	for(int i=0; i < c->dim-1; i++){
		c->fitness += (1-c->x[i])*(1-c->x[i]) + 100*((c->x[i+1]-(c->x[i]*c->x[i])) * (c->x[i+1]-(c->x[i]*c->x[i])));
	}
	return c->fitness;
}

fitness_function* getRastrigin(int nDim){
	return createFitFuncStruct("rastrigin", &rastrigin, -5.12, 5.12, 0, nDim, 0);
}
double rastrigin(candidate *c){
	// 10 * n + sum(i=1)(n) x_i^2 - 10 * cos(2 * pi * x_i): Global Best at (0,0)=0, Range[-5.12, 5.12]
	double s = 0;
	for(int i=0; i < c->dim; i++){
		s += c->x[i] * c->x[i] - 10 * cos(2 * pi * c->x[i]);
	}
	c->fitness = 10 * c->dim + s;

	return c->fitness;
}

fitness_function* getMatyas(int nDim){
	return createFitFuncStruct("matyas", &matyas, -10, 10, 0, nDim, 0);
}
double matyas(candidate *c){
	// 0.26 * (x^2 + y^2) - 0.48 * x * y : Global Best at (0,0)=0, Range[-10, 10]
//	c->fitness = 0.26 * (c->x[0]*c->x[0] + c->x[1]*c->x[1]) - 0.48 * c->x[0] * c->x[1] ;
	double s = 0;
	double p = 1;
	for(int i=0; i < c->dim; i++){
		s += c->x[i]*c->x[i];
		p *= c->x[i];
	}
	c->fitness = 0.26 * s - 0.48 * p;

	return c->fitness;
}

fitness_function* getLevi(int nDim){
	return createFitFuncStruct("levi", &levi, -10, 10, 1, nDim, 0);
}
double levi(candidate *c){
	// sin(3 * pi * x)^2 + (x - 1)^2 * (1 + sin(3 * pi * y)^2) + (y - 1)^2 * (1 + sin(2 * pi * y)^2): Global Best at (1,1)=0, Range[-10, 10]
	c->fitness = (sin(3 * pi * c->x[0])*sin(3 * pi * c->x[0]))
			+ ((c->x[0] - 1)*(c->x[0] - 1)) * (1 + (sin(3 * pi * c->x[1])*sin(3 * pi * c->x[1])))
			+ ((c->x[1] - 1)*(c->x[1] - 1)) * (1 + (sin(2 * pi * c->x[1])*sin(2 * pi * c->x[1])));
	return c->fitness;
}

fitness_function* getSchaffer2(int nDim){
	return createFitFuncStruct("schaffer2", &schaffer2, -100, 100, 0, nDim, 0);
}
double schaffer2(candidate *c){
	// 0.5 + ((sin(x^2 - y^2)^2 - 0.5) / (1 + 0.0001 * (x^2 + y^2))^2) : Global Best at (0,0)=0, Range[-100, 100]
	c->fitness = 0.5 + ((pow(sin(c->x[0]*c->x[0] - c->x[1]*c->x[1]), 2) - 0.5)
			/ pow(1 + 0.0001 * (c->x[0]*c->x[0] + c->x[1]*c->x[1]), 2));
	return c->fitness;
}

fitness_function* getAckleys(int nDim){
	return createFitFuncStruct("ackleys", &ackleys, -30, 30, 0, nDim, 0);
}
double ackleys(candidate *c){
	// -20 * exp(-0.2 * sqrt(0.5 * (x^2 + y^2))) - exp(0.5 * (cos(2 * pi * x) + cos(2 * pi * y))) + e + 20 : Global Best at (0,0)=0, Range[-30, 30]
	double s1 = 0, s2 = 0;
	for(int i=0; i < c->dim; i++){
		s1 += c->x[i]*c->x[i];
		s2 += cos(2 * pi * c->x[i]);
	}
	c->fitness = -20 * exp(-0.2 * sqrt(s1 / c->dim))- exp(s2 / c->dim) + exp(1) + 20;
	return c->fitness;
}

fitness_function* getSphere(int nDim){
	return createFitFuncStruct("sphere", &sphere, -2.048, 2.048, 0, nDim, 0);
}
double sphere(candidate *c){
	// sum(i=1)(n)x_i^2: Global Best at (0,.. 0)=0
	c->fitness = 0;
	for(int i=0; i < c->dim; i++){
		c->fitness += (c->x[i])*(c->x[i]);
	}
	return c->fitness;
}

fitness_function* getCEC15_F1(int nDim){
	return createFitFuncStruct("F1 [Shifted Elliptic Function]", new F1(), -100, 100, 0, nDim, 0);
}

fitness_function* getCEC15_F2(int nDim){
	return createFitFuncStruct("F2 [Shifted Rastrigin’s Function]", new F2(), -5, 5, 0, nDim, 0);
}

fitness_function* getCEC15_F3(int nDim){
	return createFitFuncStruct("F3 [Shifted Ackley’s Function]", new F3(), -32, 32, 0, nDim, 0);
}

fitness_function* getCEC15_F4(int nDim){
	return createFitFuncStruct("F4 [7-nonseparable, 1-separable Shifted and Rotated Elliptic Function]", new F4(), -100, 100, 0, nDim, 0);
}

fitness_function* getCEC15_F5(int nDim){
	return createFitFuncStruct("F5 [7-nonseparable, 1-separable Shifted and Rotated Rastrigin’s Function]", new F5(), -5, 5, 0, nDim, 0);
}

fitness_function* getCEC15_F6(int nDim){
	return createFitFuncStruct("F6 [7-nonseparable, 1-separable Shifted and Rotated Ackley’s Function]", new F6(), -32, 32, 0, nDim, 0);
}

fitness_function* getCEC15_F7(int nDim){
	return createFitFuncStruct("F7 [7-nonseparable, 1-separable Shifted Schwefel’s Function]", new F7(), -100, 100, 0, nDim, 0);
}

fitness_function* getCEC15_F8(int nDim){
	return createFitFuncStruct("F8 [20-nonseparable Shifted and Rotated Elliptic Function]", new F8(), -100, 100, 0, nDim, 0);
}

fitness_function* getCEC15_F9(int nDim){
	return createFitFuncStruct("F9 [20-nonseparable Shifted and Rotated Rastrigin’s Function]", new F9(), -5, 5, 0, nDim, 0);
}

fitness_function* getCEC15_F10(int nDim){
	return createFitFuncStruct("F10 [20-nonseparable Shifted and Rotated Ackley’s Function]", new F10(), -32, 32, 0, nDim, 0);
}

fitness_function* getCEC15_F11(int nDim){
	return createFitFuncStruct("F11 [20-nonseparable Shifted Schwefel’s Function]", new F11(), -100, 100, 0, nDim, 0);
}

fitness_function* getCEC15_F12(int nDim){
	return createFitFuncStruct("F12 [Shifted Rosenbrock’s Function]", new F12(), -100, 100, 0, nDim, 0);
}

fitness_function* getCEC15_F13(int nDim){
	return createFitFuncStruct("F13 [Shifted Schwefel’s Function with Conforming Overlapping Subcomponents]", new F13(), -100, 100, 0, nDim, 0);
}

fitness_function* getCEC15_F14(int nDim){
	return createFitFuncStruct("F14 [Shifted Schwefel’s Function with Conflicting Overlapping Subcomponents]", new F14(), -100, 100, 0, nDim, 0);
}

fitness_function* getCEC15_F15(int nDim){
	return createFitFuncStruct("F15 [Shifted Schwefel’s Function]", new F15(), -100, 100, 0, nDim, 0);
}
