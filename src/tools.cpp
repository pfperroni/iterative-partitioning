/**
 * Iterative Partitioning (c)
 *
 * Copyright 2015-2020 Peter Frank Perroni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional notifications, please check the file NOTICE.
 *
 *
 * @file tools.cpp
 * @author Peter Frank Perroni
 * @brief Helper functions.
 * @date Apr 30, 2015
 */

#include "tools.hpp"

void addConvergence(list *l, int nEvals, double fitness){
	if(l == NULL) return;
	convergence_block *c;
	if(l->last == NULL || ((convergence_block*)l->last->ptr)->pos == CONVERGENCE_BLOCK_SIZE){
		c = new convergence_block;
		c->pos = 0;
		addItem(l, c);
	}
	else{
		c = (convergence_block*)l->last->ptr;
	}
	c->nEvals[c->pos] = nEvals;
	c->fitness[c->pos] = fitness;
	c->pos++;
}

/**
 * @brief Add an item to the end of the list.
 *
 * @param l The list.
 * @param ptr The object to be added.
 */
void addItem(list *l, void *ptr){
	if(l == NULL || ptr == NULL) return;
	insertItem(l, ptr, l->size);
}

/**
 * @brief Insert an item in a specific position into the list.
 *
 * @param l The list.
 * @param ptr The object to be added.
 * @param pos The position to insert the object into the list. If it is larger than the list size, add to the end of the list.
 */
void insertItem(list *l, void *ptr, int pos){
	if(l == NULL || ptr == NULL) return;
	list_item *item = new list_item;
	item->ptr = ptr;
	if(l->size == 0){ // empty list.
		item->next = item->previous = NULL;
		l->first = l->last = item;
	}
	else{
		list_item *i;
		int cont = 0;
		for(i=l->first; i != NULL && cont != pos; i=i->next) cont++;
		if(i == NULL){ // Last item of the list.
			item->previous = l->last;
			item->next = NULL;
			l->last->next = item;
			l->last = item;
		}
		else if(i == l->first){ // First item of the list.
			item->previous = NULL;
			item->next = l->first;
			l->first->previous = item;
			l->first = item;
		}
		else{ // Middle of the list.
			item->previous = i->previous;
			item->next = i;
			i->previous->next = item;
			i->previous = item;
		}
	}
	l->size++;
}

list *createConvergenceList(){
	list *l = createList();
	l->listType = LIST_ITEM_CONVERGENCE;
	return l;
}

list *createBenchmarkList(){
	list *l = createList();
	l->listType = LIST_ITEM_FITNESSFUNCTION;
	return l;
}

list *createList(){
	list *l = new list;
	l->first = l->last = NULL;
	l->size = 0;
	return l;
}

void destroyList(list *l){
	if(l == NULL) return;
	emptyList(l);
	delete l;
}

void emptyList(list *l){
	if(l == NULL) return;

	list_item *next;
	for(list_item *i=l->first; i; i=next){
		if(i->ptr != NULL){
			if(l->listType == LIST_ITEM_CONVERGENCE) delete (convergence_block*)i->ptr;
			else if(l->listType == LIST_ITEM_FITNESSFUNCTION) deleteFitFuncStruct((fitness_function*)i->ptr, ((fitness_function*)i->ptr)->dim);
		}
		next = i->next;
		delete i;
	}
	l->first = l->last = NULL;
	l->size = 0;
}

double calc_media(double *val, int tamanho){
	double media = 0;
	for(int i=0; i < tamanho; i++){
		media += val[i];
	}
	return media / tamanho;
}

double calc_variancia(double *val, int tamanho){
	if(tamanho == 1) return 0;

	double media = calc_media(val, tamanho);
	double var = 0;
	for(int i=0; i < tamanho; i++){
		var += (val[i] - media) * (val[i] - media);
	}
	return var / (tamanho - 1);
}

double calc_desvpad(double *val, int tamanho){
	return sqrt(calc_variancia(val, tamanho));
}

void sumConvergence(list *l, int *addValuesToEvals, int *addValuesToHits, double *addValuesToConv, double &worstFit, double &bestFit){
	double firstFit, lastFit;
	convergence_block *c;
	int j, k = 0;
	for(list_item *it=l->first; it; it=it->next){
		c = (convergence_block*)it->ptr;
		for(j=0; j < c->pos; j++, k++){
			addValuesToEvals[k] += c->nEvals[j];
			addValuesToConv[k] += c->fitness[j];
			addValuesToHits[k]++;
		}
	}
	firstFit = ((convergence_block*)l->first->ptr)->fitness[0];
	lastFit = ((convergence_block*)l->last->ptr)->fitness[j-1];
	worstFit = (firstFit NOT_MIN_OR_MAX worstFit) ? firstFit : worstFit;
	bestFit = (lastFit MIN_OR_MAX bestFit) ? lastFit : bestFit;
}

void copyConvergence(list *l, int *evals, double *values){
	convergence_block *c;
	int j, k = 0;
	for(list_item *it=l->first; it; it=it->next){
		c = (convergence_block*)it->ptr;
		for(j=0; j < c->pos; j++, k++){
			evals[k] = c->nEvals[j];
			values[k] += c->fitness[j];
		}
	}
}

double getDoubleParam(const char *opt, int argc, char *argv[], double defaultValue){
	for(int i=1; i < argc; i++){
		if(strncmp(opt, argv[i], strlen(opt)) == 0){
			char param[100];
			strcpy(param, argv[i]+strlen(opt));
			return atof(param);
		}
	}
	return defaultValue;
}

int getIntParam(const char *opt, int argc, char *argv[], int defaultValue){
	for(int i=1; i < argc; i++){
		if(strncmp(opt, argv[i], strlen(opt)) == 0){
			char param[100];
			strcpy(param, argv[i]+strlen(opt));
			return atoi(param);
		}
	}
	return defaultValue;
}

int getIntListParam(const char *opt, int argc, char *argv[], int *list){
	for(int i=1; i < argc; i++){
		if(strncmp(opt, argv[i], strlen(opt)) == 0){
			char param[100];
			int j, k = 0;
			for(char *c=argv[i]+strlen(opt); *c; c++){
				for(j=0; *c && *c != ','; param[j++]=*c, c++);
				param[j] = '\0';
				list[k++] = atoi(param);
				if(!*c) return k;
			}
		}
	}
	return 0;
}

char getCharParam(const char *opt, int argc, char *argv[], char defaultValue){
	for(int i=1; i < argc; i++){
		if(strncmp(opt, argv[i], strlen(opt)) == 0){
			return *(argv[i]+strlen(opt));
		}
	}
	return defaultValue;
}

void getCharAndDoubleParam(const char *opt, int argc, char *argv[], char *c, double *v){
	for(int i=1; i < argc; i++){
		if(strncmp(opt, argv[i], strlen(opt)) == 0){
			*c = *(argv[i]+strlen(opt));
			char param[100];
			strcpy(param, argv[i]+strlen(opt)+1);
			*v = atof(param);
			return;
		}
	}
}

/**
 * Get real random numbers from /dev/random.
 * This device can take a while to respond. If it is taking too long,
 * change it to /dev/urandom
 */
unsigned int getRandomSeed(){
	unsigned int randVal;
	FILE *f = fopen("/dev/random", "r");
	fread(&randVal, sizeof(randVal), 1, f);
	fclose(f);
	return randVal;
}
