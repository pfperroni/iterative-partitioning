/**
 * Iterative Partitioning (c)
 *
 * Copyright 2015-2020 Peter Frank Perroni
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional notifications, please check the file NOTICE.
 *
 *
 * @file bench_func.hpp
 * @author Peter Frank Perroni
 * @brief Implementation of benchmark functions.
 * @date Apr 30, 2015
 */

#ifndef _BENCH_FUNC_HPP_
#define _BENCH_FUNC_HPP_

#include "tools.hpp"
#include <math.h>

double callF(fitness_function *f, candidate *candidate);
fitness_function* createFitFuncStruct(const char *name, double (*f)(candidate*), Benchmarks *F, double rangeMin, double rangeMax, double bestKnownPos, int nDim, double bestKnownValue);
fitness_function* createFitFuncStruct(const char *name, double (*f)(candidate*), double rangeMin, double rangeMax, double bestKnownPos, int nDim, double bestKnownValue);
fitness_function* createFitFuncStruct(const char *name, Benchmarks *F, double rangeMin, double rangeMax, double bestKnownPos, int nDim, double bestKnownValue);
void deleteFitFuncStruct(fitness_function *ff, int nDim);

fitness_function* getRosenbrock(int nDim);
double rosenbrock(candidate *c);

fitness_function* getRastrigin(int nDim);
double rastrigin(candidate *c);

fitness_function* getMatyas(int nDim);
double matyas(candidate *c);

fitness_function* getLevi(int nDim);
double levi(candidate *c);

fitness_function* getEasom(int nDim);
double easom(candidate *c);

fitness_function* getSchaffer2(int nDim);
double schaffer2(candidate *c);

fitness_function* getAckleys(int nDim);
double ackleys(candidate *c);

fitness_function* getSphere(int nDim);
double sphere(candidate *c);

fitness_function* getCEC15_F1(int nDim);
fitness_function* getCEC15_F2(int nDim);
fitness_function* getCEC15_F3(int nDim);
fitness_function* getCEC15_F4(int nDim);
fitness_function* getCEC15_F5(int nDim);
fitness_function* getCEC15_F6(int nDim);
fitness_function* getCEC15_F7(int nDim);
fitness_function* getCEC15_F8(int nDim);
fitness_function* getCEC15_F9(int nDim);
fitness_function* getCEC15_F10(int nDim);
fitness_function* getCEC15_F11(int nDim);
fitness_function* getCEC15_F12(int nDim);
fitness_function* getCEC15_F13(int nDim);
fitness_function* getCEC15_F14(int nDim);
fitness_function* getCEC15_F15(int nDim);

#endif /* _BENCH_FUNC_HPP_ */
