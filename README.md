# Iterative Partitioning

#### When using this software, or any work derived from this software, to generate scientific outcomes, either in executable, source code or output data formats, you shall reference the respective publication:
[Automated iterative partitioning for cooperatively coevolving particle swarms in large scale optimization](https://ieeexplore.ieee.org/document/7423909)

     PERRONI, Peter Frank; WEINGAERTNER, Daniel; DELGADO, Myriam Regattieri.
     Automated iterative partitioning for cooperatively coevolving particle
     swarms in large scale optimization. In: 2015 Brazilian Conference on
     Intelligent Systems (BRACIS). IEEE, 2015. p. 19-24.

